/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
var feetChecked = true;
var nauiChecked = true;
var divetimes = [0];
var divedepths = [0];
var divetimescolumn = ['Time', 0];
var divedepthscolumn = ['Depth', 0];
var currentTime = 0;
var ascendingTime = 20;
var descendingTime = 30;
var rnt = 0;
var group = -1;
var diveCount = 0;
var prevDepth = 0;
var sitHeader = document.getElementById("sitHeader").className += " hidden";
var sitInput = document.getElementById("surface_interval");
sitInput.setAttribute("type", "hidden");
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function loadNaui() {
    //window.alert("NAUI Table Loaded");
    tableGraph('table1.csv', 150);
    groups = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
    meters = [12,15,18,21,24,27,30,33,36,40];
    feet =   [40,50,60,70,80,90,100,110,120,130];

    if(feetChecked){
        setFeet();
    }
    else{
        setMeters();
    }
    nauiChecked = true;
    time1 = [5, 15, 25, 30, 40, 50, 70, 80, 100, 110, 130, 150];
    time2 = [0, 10, 15, 25, 30, 40, 50, 60, 70, 80, 80, 100];
    time3 = [0, 10, 15, 20, 25, 30, 40, 50, 55, 60, 60, 80];
    time4 = [0, 5, 10, 15, 20, 30, 35, 40, 45, 50, 60, 70];
    time5 = [0, 5, 10, 15, 20, 25, 30, 35, 40, 40, 50, 60];
    time6 = [0, 5, 10, 12, 15, 20, 25, 30, 30, 40, 40, 50];
    time7 = [0, 5, 7, 10, 15, 20, 22, 25, 25, 25, 40, 0];
    time8 = [0, 0, 5, 10, 13, 15, 20, 20, 20, 30, 0, 0];
    time9 = [0, 0, 5, 10, 12, 15, 15, 15, 25, 30, 0, 0];
    time10 = [0, 0, 5, 8, 10, 10, 10, 10, 10, 25, 0, 0];

    decompression1 = [0,0,0,0,0,0,0,0,0,0,0,5];
    decompression2 = [0,0,0,0,0,0,0,0,0,0,0,5];
    decompression3 = [0,0,0,0,0,0,0,0,0,5,0,7];
    decompression4 = [0,0,0,0,0,0,0,0,0,5,8,14];
    decompression5 = [0,0,0,0,0,0,0,0,5,0,10,17];
    decompression6 = [0,0,0,0,0,0,0,5,0,7,0,18];
    decompression7 = [0,0,0,0,0,0,0,5,0,0,15,0];
    decompression8 = [0,0,0,0,0,0,5,0,0,7,0,0];
    decompression9 = [0,0,0,0,0,5,0,0,6,14,0,0];
    decompression10= [0,0,0,0,5,0,0,0,0,10,0,0];

    diveTimes = [time1, time2, time3, time4, time5, time6, time7, time8, time9, time10];

    decompressionTimes = [decompression1, decompression2, decompression3, decompression4, decompression5, 
                    decompression6, decompression7, decompression8, decompression9, decompression10];

    minsit = 10;
    sitTimesNone = [1441,1441,1441,1441,1441,1441,1441,1441,1441,1441,1441,1441];
    sitTimesA = [10,201,290,349,395,426,456,480,502,531,539,553];
    sitTimesB = [0,10,100,159,205,238,266,290,313,341,349,363];
    sitTimesC = [0,0,10,70,118,149,179,201,224,243,260,276];
    sitTimesD = [0,0,0,10,55,90,120,144,165,185,202,217];
    sitTimesE = [0,0,0,0,10,46,76,102,123,141,159,174];
    sitTimesF = [0,0,0,0,0,10,41,67,90,108,124,140];
    sitTimesG = [0,0,0,0,0,0,10,37,60,80,96,110];
    sitTimesH = [0,0,0,0,0,0,0,10,34,55,72,86]; 
    sitTimesI = [0,0,0,0,0,0,0,0,10,32,50,65]; 
    sitTimesJ = [0,0,0,0,0,0,0,0,0,10,29,46]; 
    sitTimesK = [0,0,0,0,0,0,0,0,0,0,10,27]; 
    sitTimesL = [0,0,0,0,0,0,0,0,0,0,0,10]; 

    sit = [sitTimesNone,sitTimesA, sitTimesB, sitTimesC, sitTimesD, sitTimesE, sitTimesF,
            sitTimesG, sitTimesH, sitTimesI, sitTimesJ, sitTimesK, sitTimesL];

    // Table 3
    // Residual Nitrogen Times
    rnt1 = [7,17,25,37,49,61,73,87,101,116,138,161];
    rnt2 = [6,13,21,29,38,47,56,66,76,87,99,111];
    rnt3 = [5,11,17,24,30,36,44,52,61,70,79,88];
    rnt4 = [4,9,15,20,26,31,37,43,50,57,64,72];
    rnt5 = [4,8,13,18,23,28,32,38,43,48,54,61];
    rnt6 = [3,7,11,16,20,24,29,33,38,43,47,53];
    rnt7 = [3,7,10,14,18,22,26,30,34,38,0,0];
    rnt8 = [3,6,10,13,16,20,24,27,31,-1,-1,-1];
    rnt9 = [3,6,9,12,15,18,21,25,28,-1,-1,-1];
    rnt10 = [3,6,8,11,13,16,19,22,25,-1,-1,-1];

    rnttimes = [rnt1, rnt2, rnt3, rnt4, rnt5, rnt6, rnt7, rnt8, rnt9, rnt10];

    // Adjusted Max Dives Times
    adjtime1 = [123,113,105,93,81,69,57,43,29,14,0,0];
    adjtime2 = [74,67,59,51,42,33,24,14,4,0,0,0];
    adjtime3 = [50,44,38,31,25,19,11,0,0,0,0,0];
    adjtime4 = [41,36,30,25,19,14,8,0,0,0,0,0];
    adjtime5 = [31,27,22,17,12,7,0,0,0,0,0,0];
    adjtime6 = [22,18,14,9,5,0,0,0,0,0,0,0];
    adjtime7 = [19,15,12,8,4,0,0,0,0,0,0,0];
    adjtime8 = [12,9,5,0,0,0,0,0,0,-1,-1,-1];
    adjtime9 = [9,6,0,0,0,0,0,0,0,-1,-1,-1];
    adjtime10 = [5,0,0,0,0,0,0,0,0,-1,-1,-1];

    adjMaxDiveTimes = [adjtime1, adjtime2, adjtime3, adjtime4, adjtime5, adjtime6, adjtime7, adjtime8, adjtime9, adjtime10];

    nauiTests();
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function loadPadi() {
    //window.alert("PADI Table Loaded");
    //tableGraph('table2.csv', 220);

    groups = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    meters = [10,12,15,18,21,24,27,30,33,36,39,42];
    feet =   [35,40,50,60,70,80,90,100,110,120,130,140];

    if(feetChecked){
        setFeet();
    }
    else{
        setMeters();
    }
    nauiChecked = false;
    time1 = [10,19,25,29,32,36,40,44,48,52,57,62,67,73,79,85,92,100,108,117,127,139,152,168,188,205];
    time2 = [9,16,22,25,27,31,34,37,40,44,48,51,55,60,64,69,74,79,85,91,97,104,111,120,129,140];
    time3 = [7,13,17,19,21,24,26,28,31,33,36,39,41,44,47,50,53,57,60,63,67,71,75,80,0,0];
    time4 = [6,11,14,16,17,19,21,23,25,27,29,31,33,35,37,39,42,44,47,49,52,54,55,0,0,0];
    time5 = [5,9,12,13,15,16,18,19,21,22,24,26,27,29,31,33,35,36,38,40,0,0,0,0,0,0];
    time6 = [4,8,10,11,13,14,15,17,18,19,21,22,23,25,26,28,29,30,0,0,0,0,0,0,0,0];
    time7 = [4,7,9,10,11,12,13,15,16,17,18,19,21,22,23,24,25,0,0,0,0,0,0,0,0,0];
    time8 = [3,6,8,9,10,11,12,13,14,15,16,17,18,19,20,0,0,0,0,0,0,0,0,0,0,0];
    time9 = [3,6,7,8,9,10,11,12,13,13,14,15,16,0,0,0,0,0,0,0,0,0,0,0,0,0];
    time10= [3,5,6,7,8,9,10,11,11,12,13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    time11= [3,5,6,7,7,8,9,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    time12= [0,4,5,6,7,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

    decompression1 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3];
    decompression2 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3];
    decompression3 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0];
    decompression4 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0,0];
    decompression5 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0,0,0,0,0];
    decompression6 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0,0,0,0,0,0,0];
    decompression7 = [0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0];
    decompression8 = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0];
    decompression9 = [3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0];
    decompression10= [3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    decompression11= [3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    decompression12= [3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

    diveTimes = [time1, time2, time3, time4, time5, time6, time7, time8, time9, time10, time11, time12];

    decompressionTimes = [decompression1, decompression2, decompression3, decompression4, decompression5, 
                    decompression6, decompression7, decompression8, decompression9, decompression10, decompression11, decompression12];



    // Table 2
    // Dive Times per depth
    minsit = 0;
    sitTimesNone = [180,228,250,259,268,275,282,288,294,300,305,310,315,319,324,328,331,335,339,342,345,348,351,354,357,360];
    sitTimesA = [0,48,70,79,88,95,102,108,114,120,125,130,135,139,144,148,151,155,159,162,165,168,171,174,177,180];
    sitTimesB = [0,0,22,31,39,47,54,60,66,72,77,82,86,91,95,99,103,107,110,114,117,120,123,126,129,132];
    sitTimesC = [0,0,0,9,17,25,32,38,44,50,55,60,65,69,73,77,81,85,88,92,95,98,101,104,107,110];
    sitTimesD = [0,0,0,0,8,16,23,29,35,41,46,51,56,60,64,68,72,76,79,83,86,89,92,95,98,101];
    sitTimesE = [0,0,0,0,0,8,14,21,27,32,38,43,47,52,56,60,64,68,71,74,78,81,84,87,90,92];
    sitTimesF = [0,0,0,0,0,0,7,13,19,25,30,35,40,44,48,52,56,60,64,67,70,73,76,79,82,85];
    sitTimesG = [0,0,0,0,0,0,0,6,12,18,23,28,33,37,42,46,49,53,57,60,63,66,69,72,75,78];
    sitTimesH = [0,0,0,0,0,0,0,0,6,12,17,22,26,31,35,39,43,47,50,54,57,60,63,66,69,72];
    sitTimesI = [0,0,0,0,0,0,0,0,0,6,11,16,20,25,29,33,37,41,44,48,51,54,57,60,63,66];
    sitTimesJ = [0,0,0,0,0,0,0,0,0,0,5,10,15,19,24,28,31,35,39,42,45,48,51,54,57,60]; 
    sitTimesK = [0,0,0,0,0,0,0,0,0,0,0,5,10,14,18,22,26,30,33,37,40,43,46,49,52,55]; 
    sitTimesL = [0,0,0,0,0,0,0,0,0,0,0,0,5,9,13,17,21,25,28,32,35,38,41,44,47,50]; 
    sitTimesM = [0,0,0,0,0,0,0,0,0,0,0,0,0,4,9,13,17,20,24,27,30,34,37,40,42,45];
    sitTimesN = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,8,12,16,19,23,26,29,32,35,38,41];
    sitTimesO = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,8,12,15,18,22,25,28,31,34,36];
    sitTimesP = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,8,11,14,18,21,24,27,30,32];
    sitTimesQ = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,7,11,14,17,20,23,26,29];
    sitTimesR = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,7,10,13,16,19,22,25];
    sitTimesS = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,7,10,13,16,19,21];
    sitTimesT = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,6,9,12,15,18];
    sitTimesU = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6,9,12,15];
    sitTimesV = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6,9,12]; 
    sitTimesW = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6,9];
    sitTimesX = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6];
    sitTimesY = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3]; 
    sitTimesZ = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 

    sit = [sitTimesNone, sitTimesA, sitTimesB, sitTimesC, sitTimesD, sitTimesE,
            sitTimesF, sitTimesG, sitTimesH, sitTimesI, sitTimesJ, sitTimesK, sitTimesL,
            sitTimesM, sitTimesN, sitTimesO, sitTimesP, sitTimesQ, sitTimesR,
            sitTimesS, sitTimesT, sitTimesU, sitTimesV, sitTimesW, sitTimesX,
            sitTimesY, sitTimesZ];

    // Table 3
    // Residual Nitrogen Times
    rnt1 = [10,19,25,29,32,36,40,44,48,52,57,62,67,73,79,85,92,100,108,117,127,139,152,168,188,205];
    rnt2 = [9,16,22,25,27,31,34,37,40,44,48,51,55,60,64,69,74,79,85,91,97,104,111,120,129,140];
    rnt3 = [7,13,17,19,21,24,26,28,31,33,36,38,41,44,47,50,53,57,60,63,67,71,75,80,0,0];
    rnt4 = [6,11,14,16,17,19,21,23,25,27,29,31,33,35,37,39,42,44,47,49,52,54,55,0,0,0];
    rnt5 = [5,9,12,13,15,16,18,19,21,22,24,26,27,29,31,33,34,36,38,40,0,0,0,0,0,0];
    rnt6 = [4,8,10,11,13,14,15,17,18,19,21,22,23,25,26,28,29,30,0,0,0,0,0,0,0,0];
    rnt7 = [4,7,9,10,11,12,13,15,16,17,18,19,21,22,23,24,25,0,0,0,0,0,0,0,0,0];
    rnt8 = [3,6,8,9,10,11,12,13,14,15,16,17,18,19,20,0,0,0,0,0,0,0,0,0,0,0];
    rnt9 = [3,6,7,8,9,10,11,12,13,14,14,15,16,0,0,0,0,0,0,0,0,0,0,0,0,0];
    rnt10= [3,5,6,7,8,9,10,11,12,12,13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    rnt11= [3,5,6,7,8,8,9,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

    rnttimes = [rnt1, rnt2, rnt3, rnt4, rnt5, rnt6, rnt7, rnt8, rnt9, rnt10, rnt11];

    // Adjusted Max Dives Times
    adjtime1 = [195,186,180,176,173,169,165,161,157,153,148,143,138,132,126,120,113,105,97,88,78,66,53,37,17,0];
    adjtime2 = [131,124,118,115,113,109,106,103,100,96,92,89,85,80,76,71,66,61,55,49,43,36,29,20,11,0];
    adjtime3 = [73,67,63,61,59,56,54,52,49,47,44,42,39,36,33,30,27,23,20,17,13,9,5,0,0,0];
    adjtime4 = [49,44,41,39,38,36,34,32,30,28,26,24,22,20,18,16,13,11,8,6,3,1,0,0,0,0];
    adjtime5 = [35,31,28,27,25,24,22,21,19,18,16,14,13,11,9,7,6,4,3,0,0,0,0,0,0,0];
    adjtime6 = [26,22,20,19,17,16,15,13,12,11,9,8,7,5,4,2,0,0,0,0,0,0,0,0,0,0];
    adjtime7 = [21,18,16,15,14,13,12,10,9,8,7,6,4,3,2,0,0,0,0,0,0,0,0,0,0,0];
    adjtime8 = [17,14,12,11,10,9,8,7,6,5,4,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0];
    adjtime9 = [13,10,9,8,7,6,5,4,3,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    adjtime10= [10,8,7,6,5,4,3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    adjtime11= [7,5,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

    adjMaxDiveTimes = [adjtime1, adjtime2, adjtime3, adjtime4, adjtime5, adjtime6, 
                        adjtime7, adjtime8, adjtime9, adjtime10, adjtime11];

    padiTests();
}
function setFeet(){
    feetChecked = true;
    measurement = feet;
    //window.alert(measurement);
}
function setMeters(){
    feetChecked = false;
    measurement = meters;
    //window.alert(measurement);
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
var chart = c3.generate({
    bindto: '#diveGraph',
    size: {
        height: 500,
        width: 700
    },
    data: {
        x: 'Time',
        columns: [
        divetimescolumn,
        divedepthscolumn
        ],
        names: {
            Time: 'Time',
            Depth: 'Depth'
        },
        types: {
            Depth: 'area'
        },
        colors: {
            Depth: 'lightblue'
        }
    },
    point: {
        show: false
    },
    axis: {
        y: {
            inverted: true,
            min: 0,
            max: Math.max.apply(Math, divedepths),
            tick: {
                format: function(d) { 
                    return d; 
                }
            },
            label: {
                text: 'Depth (feet)',
                position: 'outer-middle'
            } 
        },
        x: {
            min: 0,
            tick: {
                values: function () {
                    var times = [];
                    for (i = 0; i <= Math.max.apply(Math, divetimes); i += 5) { 
                        times.push(i);
                    }
                    return times;
                },
                fit: true
            },
            label: {
                text: 'Time (minutes)',
                position: 'outer-center'
            }
        }
    },
    tooltip: {
        format: {
            title: function (d) { return 'Time: ' + d + ' min'; },
            value: function (d) { return d + ' feet'; }
        }
}   // Graph initialization
});
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function generateGraph(){

var currentDepth = (+document.getElementById("depth").value);
var diveTime = (+document.getElementById("time").value);
var sitTime = (+document.getElementById("surface_interval").value);
// Input validation!

// 1. Check if the fields for depth and height were entered
if(currentDepth == 0 || diveTime == 0){
    window.alert("Depth and time inputs cannot be zero");
    return
}
// 2 & 3
if(diveCount != 0){
    // 2. Sit time validation
    var newgroup = sitTimes(sitTime, group);
    if (newgroup == -1){
        window.alert("Surface interval time must be greater than "+ minsit + " minutes.");
        return;
    }
    else if(newgroup == 0){
        rnt = 0;
        var diveCountTemp = diveCount;
        var prevDepthTemp = prevDepth;
        diveCount = 0;
        prevDepth = 0;
    }
    else{
        // 3. Residual nitrogen times
        rnt = residualNitrogenTime(currentDepth, diveTime, newgroup);
        if(rnt == -1){
            window.alert("Dive depth too deep or passed adjusted max dive time");
            return;
        } 
        window.alert("Sit put you in group " + groups[newgroup - 1]);
    }
}

// 4. Check if prev depth is less than current depth
if(currentDepth > prevDepth && diveCount != 0){
    prevDepth = prevDepthTemp;
    diveCount = diveCountTemp;
    window.alert("Repetitive dive must be at a lower depth than previous dive");
    return;
}
// 5. Nitrogen group validation
var tempGroup = group;
group = nitrogenGroup(currentDepth, diveTime, rnt);
if(group == -1){
    group = tempGroup;
    window.alert("The input data has exceeded the maximum dive depth or dive time at the current depth. Please try different input values!");
    return;
}
window.alert("Dive put you in group " + groups[group]);
// Decompression time needed?
prevDepth = currentDepth;
diveCount++;
// END Input Validation

/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
// Calculating graph values

// 1. Get surface time interval and push to time array
if(diveCount != 0){
    divedepths.push(0);
    divedepthscolumn.push(0);
    currentTime += (+document.getElementById("surface_interval").value);
    divetimes.push(currentTime);
    divetimescolumn.push(currentTime);
}
// 9. Show sit time input if repetitive dive is available
if(diveCount > 0){
    sitHeader = document.getElementById("sitHeader").className -= " hidden";
    sitInput.setAttribute("type", "text");
}
else{
    sitHeader = document.getElementById("sitHeader").className += " hidden";
    sitInput.setAttribute("type", "hidden");
}

// 2. Calculate ascending and descending rate times
rateTimeDesc = (currentDepth/descendingTime);
rateTimeAsc = (currentDepth/ascendingTime);
// 3. Add user depth onto depth array
divedepths.push(currentDepth);
divedepthscolumn.push(currentDepth);

// 4. Add descending rate time and push to times array
currentTime += rateTimeDesc;
divetimes.push(currentTime);
divetimescolumn.push(currentTime);

// 5. Push current depth into depths array
divedepths.push(currentDepth);
divedepthscolumn.push(currentDepth);

// 6. Add user time to current time and push new time into times array
currentTime += (+document.getElementById("time").value - rateTimeDesc - rateTimeAsc);
divetimes.push(currentTime);
divetimescolumn.push(currentTime);

// 7. Check for decompression times
var currentDecompression = decompression(currentDepth, diveTime, rnt);
if(currentDecompression > 0){
    divedepths.push(15);
    divedepthscolumn.push(15);
    rateTimeAsc = (currentDepth - 15)/ascendingTime;
    currentTime += rateTimeAsc;
    divetimes.push(currentTime);
    divetimescolumn.push(currentTime);

    divedepths.push(15);
    divedepthscolumn.push(15);
    currentTime += currentDecompression;
    divetimes.push(currentTime);
    divetimescolumn.push(currentTime);

    divedepths.push(0);
    divedepthscolumn.push(0);
    rateTimeAsc = 15/ascendingTime;
    currentTime += rateTimeAsc;
    divetimes.push(currentTime);
    divetimescolumn.push(currentTime);
}
// 7. Back to surface (currently not checking for decompression times)
else{
    divedepths.push(0);
    divedepthscolumn.push(0);
    currentTime += rateTimeAsc;
    divetimes.push(currentTime);
    divetimescolumn.push(currentTime);
}

prevDepth = currentDepth;


/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/

document.getElementById("dive").reset();
var chart = c3.generate({
    bindto: '#diveGraph',
    size: {
        height: 500,
        width: 700
    },
    data: {
        x: 'Time',
        columns: [
        divetimescolumn,
        divedepthscolumn
        ],
        names: {
            Time: 'Time',
            Depth: 'Depth'
        },
        types: {
            Depth: 'area'
        },
        colors: {
            Depth: 'lightblue'
        }
    },
    point: {
        show: true
    },
    axis: {
        y: {
            inverted: true,
            min: 0,
            max: Math.max.apply(Math, divedepths),
            tick: {
                format: function(d) { 
                    return d; 
                }
            },
            label: {
                text: 'Depth (feet)',
                position: 'outer-middle'
            } 
        },
        x: {
            min: 0,
            tick: {
                values: function () {
                    var times = [];
                    for (i = 0; i <= Math.max.apply(Math, divetimes); i += 5) { 
                        times.push(i);
                    }
                    return times;
                },
                fit: true
            },
            label: {
                text: 'Time (minutes)',
                position: 'outer-center'
            }
        }
    },
    tooltip: {
        format: {
            title: function (d) { return 'Time: ' + d + ' min'; },
            value: function (d) { return d + ' feet'; }
        }
    }
    });
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/