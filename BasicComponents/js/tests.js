/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
var feet = [40, 50, 60, 70, 80, 90, 100, 110, 120, 130];
var meters = [12,15,18,21,24,27,30,33,36,40];
var groups = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
var measurement = feet;

var time1 = [5, 15, 25, 30, 40, 50, 70, 80, 100, 110, 130, 150];
var time2 = [0, 10, 15, 25, 30, 40, 50, 60, 70, 80, 80, 100];
var time3 = [0, 10, 15, 20, 25, 30, 40, 50, 55, 60, 60, 80];
var time4 = [0, 5, 10, 15, 20, 30, 35, 40, 45, 50, 60, 70];
var time5 = [0, 5, 10, 15, 20, 25, 30, 35, 40, 40, 50, 60];
var time6 = [0, 5, 10, 12, 15, 20, 25, 30, 30, 40, 40, 50];
var time7 = [0, 5, 7, 10, 15, 20, 22, 25, 25, 25, 40, 0];
var time8 = [0, 0, 5, 10, 13, 15, 20, 20, 20, 30, 0, 0];
var time9 = [0, 0, 5, 10, 12, 15, 15, 15, 25, 30, 0, 0];
var time10 = [0, 0, 5, 8, 10, 10, 10, 10, 10, 25, 0, 0];
var time11;
var time12;

var decompression1 = [0,0,0,0,0,0,0,0,0,0,0,5];
var decompression2 = [0,0,0,0,0,0,0,0,0,0,0,5];
var decompression3 = [0,0,0,0,0,0,0,0,0,5,0,7];
var decompression4 = [0,0,0,0,0,0,0,0,0,5,8,14];
var decompression5 = [0,0,0,0,0,0,0,0,5,0,10,17];
var decompression6 = [0,0,0,0,0,0,0,5,0,7,0,18];
var decompression7 = [0,0,0,0,0,0,0,5,0,0,15,0];
var decompression8 = [0,0,0,0,0,0,5,0,0,7,0,0];
var decompression9 = [0,0,0,0,0,5,0,0,6,14,0,0];
var decompression10= [0,0,0,0,5,0,0,0,0,10,0,0];
var decompression11;
var decompression12;

var diveTimes = [time1, time2, time3, time4, time5, time6, time7, time8, time9, time10];

var decompressionTimes = [decompression1, decompression2, decompression3, decompression4, decompression5, 
                decompression6, decompression7, decompression8, decompression9, decompression10];

var minsit = 10;
var sitTimesNone = [1441,1441,1441,1441,1441,1441,1441,1441,1441,1441,1441,1441];
var sitTimesA = [10,201,290,349,395,426,456,480,502,531,539,553];
var sitTimesB = [0,10,100,159,205,238,266,290,313,341,349,363];
var sitTimesC = [0,0,10,70,118,149,179,201,224,243,260,276];
var sitTimesD = [0,0,0,10,55,90,120,144,165,185,202,217];
var sitTimesE = [0,0,0,0,10,46,76,102,123,141,159,174];
var sitTimesF = [0,0,0,0,0,10,41,67,90,108,124,140];
var sitTimesG = [0,0,0,0,0,0,10,37,60,80,96,110];
var sitTimesH = [0,0,0,0,0,0,0,10,34,55,72,86]; 
var sitTimesI = [0,0,0,0,0,0,0,0,10,32,50,65]; 
var sitTimesJ = [0,0,0,0,0,0,0,0,0,10,29,46]; 
var sitTimesK = [0,0,0,0,0,0,0,0,0,0,10,27]; 
var sitTimesL = [0,0,0,0,0,0,0,0,0,0,0,10]; 
var sitTimesM;
var sitTimesN;
var sitTimesO;
var sitTimesP;
var sitTimesQ;
var sitTimesR;
var sitTimesS;
var sitTimesT;
var sitTimesU;
var sitTimesV;
var sitTimesW;
var sitTimesX;
var sitTimesY;
var sitTimesZ;

var sit = [sitTimesNone,sitTimesA, sitTimesB, sitTimesC, sitTimesD, sitTimesE, sitTimesF,
		sitTimesG, sitTimesH, sitTimesI, sitTimesJ, sitTimesK, sitTimesL];

// Table 3
// Residual Nitrogen Times
var rnt1 = [7,17,25,37,49,61,73,87,101,116,138,161];
var rnt2 = [6,13,21,29,38,47,56,66,76,87,99,111];
var rnt3 = [5,11,17,24,30,36,44,52,61,70,79,88];
var rnt4 = [4,9,15,20,26,31,37,43,50,57,64,72];
var rnt5 = [4,8,13,18,23,28,32,38,43,48,54,61];
var rnt6 = [3,7,11,16,20,24,29,33,38,43,47,53];
var rnt7 = [3,7,10,14,18,22,26,30,34,38,0,0];
var rnt8 = [3,6,10,13,16,20,24,27,31,-1,-1,-1];
var rnt9 = [3,6,9,12,15,18,21,25,28,-1,-1,-1];
var rnt10 = [3,6,8,11,13,16,19,22,25,-1,-1,-1];
var rnt11;

var rnttimes = [rnt1, rnt2, rnt3, rnt4, rnt5, rnt6, rnt7, rnt8, rnt9, rnt10, rnt11];

// Adjusted Max Dives Times
var adjtime1 = [123,113,105,93,81,69,57,43,29,14,0,0];
var adjtime2 = [74,67,59,51,42,33,24,14,4,0,0,0];
var adjtime3 = [50,44,38,31,25,19,11,0,0,0,0,0];
var adjtime4 = [41,36,30,25,19,14,8,0,0,0,0,0];
var adjtime5 = [31,27,22,17,12,7,0,0,0,0,0,0];
var adjtime6 = [22,18,14,9,5,0,0,0,0,0,0,0];
var adjtime7 = [19,15,12,8,4,0,0,0,0,0,0,0];
var adjtime8 = [12,9,5,0,0,0,0,0,0,-1,-1,-1];
var adjtime9 = [9,6,0,0,0,0,0,0,0,-1,-1,-1];
var adjtime10 = [5,0,0,0,0,0,0,0,0,-1,-1,-1];
var adjtime11;

var adjMaxDiveTimes = [adjtime1, adjtime2, adjtime3, adjtime4, adjtime5, adjtime6, adjtime7, adjtime8, adjtime9, adjtime10, adjtime11];
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function depth(depth){
    // Check for invalid depth
    if(depth <= 0){
        return -1;
    }
    // Find correct depth
    var i = 0;
    while (i < measurement.length) {
        if (depth > measurement[i]) {
            i++;
        } 
        else {
            break;
        }
    }

    // Check if dive is out of scope of the current table
    if (i >= measurement.length) {
        //"Too deep to dive"
        return -1;
    }
    return measurement[i];
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function time(depthgroup, time){
    // Check for invalid time at this depth
    if(time <= 0){
        return -1;
    }
    if(depthgroup < 0 || depthgroup >= diveTimes.length){
        return -1;
    }
    // Using i, we can now find the array that holds the times for that depth
    var j = 0;
    while (j < groups.length) {
        if (time > diveTimes[depthgroup][j]) {
            j++;
        } else {
            break;
        }
    }

    // Check if dive is out of scope of the current table
    if (j >= groups.length || diveTimes[depthgroup][j] === 0) {
        //"Too much time at that depth"
        return -1;
    }
    return diveTimes[depthgroup][j];
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function nitrogenGroup(depth, time, rnt) {
    // Find correct depth
    var i = 0;
    while (i < measurement.length) {
        if (depth > measurement[i]) {
            i++;
        } 
        else {
            break;
        }
    }

    // Check if dive is out of scope of the current table
    if (i >= measurement.length) {
        //"Too deep to dive"
        return -1;
    }

    // Using i, we can now find the array that holds the times for that depth
    var j = 0;
    time += rnt;
    while (j < groups.length) {
        if (time > diveTimes[i][j]) {
            j++;
        } else {
            break;
        }
    }

    // Check if dive is out of scope of the current table
    if (j >= groups.length) {
        //"Too much time at that depth"
        return -1;
    }
    // Nitrogen Group value
    var group = groups[j];
    return j;
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function decompression(depth, time, rnt){
    // Find correct depth
    var i = 0;
    if(depth == 0 || time == 0){
        return -1;
    }
    while (i < measurement.length) {
        if (depth > measurement[i]) {
            i++;
        } 
        else {
            break;
        }
    }

    // Check if dive is out of scope of the current table
    if (i >= measurement.length) {
        //"Too deep to dive"
        return -1;
    }

    // Using i, we can now find the array that holds the times for that depth
    var j = 0;
    time += rnt;
    while (j < groups.length) {
        if (time > diveTimes[i][j]) {
            j++;
        } else {
            break;
        }
    }

    // Check if dive is out of scope of the current table
    if (j >= groups.length) {
        //"Too much time at that depth"
        return -1;
    }

    return decompressionTimes[i][j];
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function sitTimes(time, group){
    if(group >= groups.length){
        return -1;
    }
	var i = 0;
	while(i < sit.length){
		if(time < sit[i][group]){
			i++;
		}
		else{
			break;
		}
	}

	// Check if sit time is out of scope of the sit table
    if(i >= sit.length || sit[i][group] < minsit){
		//"SIT must be greater than " + minsit + " minutes"
        return -1;
    }

	// Value of i is the new group for the next dive
	// var newgroup = group[i];
	return i;
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function residualNitrogenTime(depth, time, startgroup){
	// Residual Nitrogen time
	var rnt = 0;
    // Adjusted Max Dive time
	var amt = 0;
	// If value of i is 0, then the current dive is not a repetitive dive
	if(startgroup === 0){
		//"Not a repetitive dive. Can use table 1 as a fresh dive"
        rnt = 0;
        return rnt;
	}
    // Last set of groups had '-' as a group in the 2D array
    // Subtract 1 from index to match correct groups
    startgroup--;
    if(startgroup >= groups.length){
        return -1;
    }
	var j = 0;
    if(nauiChecked){
        while(j < measurement.length){
            if(depth > measurement[j]){
                j++;
            }
            else{
                break;
            }
        }
        if(j >= measurement.length){
            //"Too deep to dive"
            return -1;
        }
        else if(time > adjMaxDiveTimes[j][startgroup]){
            //"Passed max dive time for this depth in a repeated dive"
            return -1;
        }
        else{
            rnt = rnttimes[j][startgroup];
        }
    }
    else{
        while(j < measurement.length - 1){
            if(depth > measurement[j]){
                j++;
            }
            else{
                break;
            }
        }
        if(j >= measurement.length - 1){
            //"Too deep to dive"
            return -1;
        }
        else if(time > adjMaxDiveTimes[j][startgroup]){
            //"Passed max dive time for this depth in a repeated dive"
            return -1;
        }
        else{
            rnt = rnttimes[j][startgroup];
        }
    }


	// rnt is the value that will be added to the time input from user for repetitive dive
	return rnt;

// Continue back to dive table 1
}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function repetitiveDives(depth1, time1, sitTime, depth2, time2){
    // First dive
    var firstGroup = nitrogenGroup(depth1, time1, 0);
    if(firstGroup < 0){
        return notOk(!firstGroup < 0, "First nitrogen group did not succeed");
    }
    // Check for correct dive depths
    if(depth2 > depth1 && sitTime < sit[0][firstGroup]){
        return notOk(!(depth2 > depth1 && sitTime < sit[0][firstGroup]), "Second dive depth must be less than the first dive depth");
    }
    // Check if interval time is needed
    if(sitTime >= sit[0][firstGroup]){
        secondDive = nitrogenGroup(depth2, time2, 0);
            if(secondDive < 0){
                return notOK(secondDive < 0, "Second Dive not successful. No sit time")
            }
            else{
                return ok(secondDive >= 0, "Second nitrogen group ok. No sit time ");
            }
    }
    // Get new group
    var newGroup = sitTimes(sitTime, firstGroup);
    if(newGroup < 0){
        return notOk(newGroup >= 0, "Sit time not long enough");
    }
    // Get nitrogen time
    var resTime = residualNitrogenTime(depth2, time2, newGroup);
    if(resTime < 0){
        return notOk(resTime >= 0, "Passed max dive or too deep");      
    }

    // Get group of repetitive dive
    var secondDive = nitrogenGroup(depth2, time2, resTime);
    if(secondDive < 0){
        return notOK(secondDive >= 0, "Second Dive not successful")
    }
    else{
        return ok(secondDive >= 0, "Second nitrogen group ok");
    }
}
//loadNaui();
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function nauiTests(){
    //loadNaui();
    QUnit.test('depth()', function (assert) {
        equal(depth(-1), -1, "Not a proper dive depth");
        equal(depth(0), -1, "Not a proper dive depth");
        equal(depth(40), 40, "Depth 40");
        equal(depth(61), 70, "Depth 70");
        equal(depth(89), 90, "Depth 90");
        equal(depth(129), 130, "Depth 130");
        equal(depth(130), 130, "Depth 130");
        equal(depth(131), -1, "Not a proper dive depth");
    });
    QUnit.test('time()', function (assert){
        equal(time(0,0), -1, "Cannot dive for zero minutes");
        equal(time(5,-2), -1, "Cannot dive for negative minutes");
        equal(time(1,85), 100, "Passed max w/o decompression");
        equal(time(2,23), 25, "23 is less than 25");
        equal(time(3,56), 60, "56 is less than 60");
        equal(time(4,2), 5, "2 is less than 5");
        equal(time(5,35), 40, "35 is less than 40");
        equal(time(6,22), 22, "22 = 22");
        equal(time(7,12), 13, "12 is less than 13");
        equal(time(8,32), -1, "Passed max time");
        equal(time(9,3), 5, "3 is less than 5");
    });
    QUnit.test('decompression()', function (assert){
        equal(decompression(40, 95, 0), 0);
        equal(decompression(50, 85, 0), 5);
        equal(decompression(60, 57, 0), 5);
        equal(decompression(60, 61, 0), 7);
        equal(decompression(70, 57, 0), 8);
        equal(decompression(70, 75, 0), -1);
        equal(decompression(80, 22, 0), 0);
        equal(decompression(90, 41, 0), 18);
        equal(decompression(100, 38, 0), 15);
        equal(decompression(110, 30, 0), 7);
        equal(decompression(110, 31, 0), -1);
        equal(decompression(120, 12, 0), 0);
        equal(decompression(120, 15, 0), 5);
        equal(decompression(120, 16, 0), 6);
        equal(decompression(130, 11, 0), 10);
        equal(decompression(130, 26, 0), -1);
        equal(decompression(131, 14, 0), -1);
    });
    QUnit.test('nitrogenGroup()', function (assert) {
        equal(nitrogenGroup(5,5,0), 0, "Group A");
        equal(nitrogenGroup(80,12,0), 3, 'Group D');
        equal(nitrogenGroup(60,27,0), 5, 'Group F');
        equal(nitrogenGroup(100,26,0), 10, 'Group K');
        equal(nitrogenGroup(150,13,0), -1, 'Too Deep!');
    });

    QUnit.test('sitTimes()', function (assert) {
        equal(sitTimes(5,0), -1, "Prev Group A, Not long enough sit time! Min SIT time is 10");
        equal(sitTimes(85,3), 3, 'Prev Group D, New Group C');
        equal(sitTimes(30,5), 6, 'Prev Group F, New Group F');
        equal(sitTimes(538,10), 2, 'Prev Group K, New Group B');
        equal(sitTimes(6000,11), 0, 'Prev Group L - New Group No group, fresh dive');
    });

    QUnit.test('residualNitrogenTime()', function (assert) {
        equal(residualNitrogenTime(85,25,1), -1, "Prev Group A, Passed max dive time!");
        equal(residualNitrogenTime(62,20,4), 20, 'Prev Group D');
        equal(residualNitrogenTime(48,13,6), 47, 'Prev Group F');
        equal(residualNitrogenTime(50,10,11), -1, 'Prev Group K,no max time given!');
        equal(residualNitrogenTime(120,11,0), 0, 'No rnt, fresh dive. Check table 1');
        equal(residualNitrogenTime(65,15,4), 20);
    });
    QUnit.test('repetitiveDives()', function (assert) {
        repetitiveDives(65,17,62,65,15);
        repetitiveDives(40,100,180,110,10);
        //repetitiveDives(100,42,300, 45, 60);
        repetitiveDives(100, 22, 1441, 90,35);
        repetitiveDives(110, 13, 180, 110, 15);
    });
}
//loadPadi();
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function padiTests(){
    //loadPadi();
    QUnit.test('depth()', function (assert) {
        equal(depth(-1), -1, "Not a proper dive depth");
        equal(depth(0), -1, "Not a proper dive depth");
        equal(depth(34), 35, "Depth 35");
        equal(depth(35), 35, "Depth 35");
        equal(depth(81), 90, "Depth 90");
        equal(depth(129), 130, "Depth 130");
        equal(depth(130), 130, "Depth 130");
        equal(depth(141), -1, "Not a proper dive depth");
    });
    QUnit.test('time()', function (assert){
        equal(time(0,0), -1);
        equal(time(0,9), 10);
        equal(time(0,11), 19);
        equal(time(1,0), -1);
        equal(time(0,205), 205);
        equal(time(0,206), -1);
        equal(time(1,130), 140);
        equal(time(1,141), -1);
        equal(time(4,19), 19);
        equal(time(5,20), 21);
        equal(time(6,16), 16);
        equal(time(10,2), 3);
        equal(time(10,10), 10);
        equal(time(11,5), 5);
        equal(time(11,9),-1);
        equal(time(12,7),-1);
    });
    QUnit.test('decompression()', function (assert){
        equal(decompression(0, 0, 0), -1);
        equal(decompression(35, 160, 0), 3);
        equal(decompression(35, 215, 0), -1);
        equal(decompression(50, 70, 0), 3);
        equal(decompression(60, 30, 0), 0);
        equal(decompression(70, 75, 0), -1);
        equal(decompression(80, 22, 0), 0);
        equal(decompression(100, 5, 0), 3);
        equal(decompression(100, 21, 0), -1);
        equal(decompression(110, 10, 0), 3);
        equal(decompression(130, 7,0), 3);
        equal(decompression(140, 9, 0), -1);
    });
    QUnit.test('nitrogenGroup()', function (assert) {
        equal(nitrogenGroup(5,5,0), 0);
        equal(nitrogenGroup(128,3,0), 0);
        equal(nitrogenGroup(15,17,0), 1);
        equal(nitrogenGroup(136,3,0), 1);
        equal(nitrogenGroup(105,15,0),11);
        equal(nitrogenGroup(48,37,0), 11);
        equal(nitrogenGroup(66,28,0), 13);
        equal(nitrogenGroup(110,18,0), -1);
        equal(nitrogenGroup(20,170,0), 24);
        equal(nitrogenGroup(36,125,0), 24);
        equal(nitrogenGroup(5,200,0), 25);
        equal(nitrogenGroup(38,130,0), 25);
        equal(nitrogenGroup(45,82,0), -1);
    });

    QUnit.test('sitTimes()', function (assert) {
        equal(sitTimes(5,0), 1);
        equal(sitTimes(65,3), 2);
        equal(sitTimes(30,6), 4);
        equal(sitTimes(538,9), 0);
        equal(sitTimes(60,12), 4);
        equal(sitTimes(14,15), 13);
        equal(sitTimes(85,18), 4);
        equal(sitTimes(30,21), 14);
        equal(sitTimes(7,24), 23);
        equal(sitTimes(6000,27), -1);
    });

    QUnit.test('residualNitrogenTime()', function (assert) {
        equal(residualNitrogenTime(130,5,1), 3);
        equal(residualNitrogenTime(140,3,1), -1);
        equal(residualNitrogenTime(130,5,2), 5);
        equal(residualNitrogenTime(130,5,0), 0);

        equal(residualNitrogenTime(34,100,1), 10);
        equal(residualNitrogenTime(36,120,2), 16);
        equal(residualNitrogenTime(41,60,2), 13);

        equal(residualNitrogenTime(30,4,26), -1);
        equal(residualNitrogenTime(51,15,26), -1);
        equal(residualNitrogenTime(34,15,25), 188);
        equal(residualNitrogenTime(36,11,25), 129);

        equal(residualNitrogenTime(70,4,20), -1);
        equal(residualNitrogenTime(90,25,23), -1);
        equal(residualNitrogenTime(80,2,16), 28);
        equal(residualNitrogenTime(95,2,12), 17);
        equal(residualNitrogenTime(140,10,1), -1);
        equal(residualNitrogenTime(120,3,6), 9);
    });
    QUnit.test('repetitiveDives()', function (assert) {
        repetitiveDives(65,17,62,65,15);
        repetitiveDives(40,100,180,110,10);
        repetitiveDives(100,42,300, 45, 60);
        repetitiveDives(100, 22, 1441, 90,35);
        repetitiveDives(110, 13, 180, 110, 15);
    });
}
//nauiTests();
//padiTests();
