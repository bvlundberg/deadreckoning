/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/
function tableGraph(file, max){
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
      width = 960 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

  var x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .1);

  var y = d3.scale.linear()
      .rangeRound([height, 0]);

  var tooltip = d3.select('body').append('div')
      .style('position', 'absolute')
      .style('padding', '0 10px')
      .style('background', 'white')
      .style('opacity', 0)

  var color = d3.scale.ordinal()
      .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .tickFormat(d3.format(".2s"));

  var svg = d3.select("body").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  d3.csv("BasicComponents/csv/" + file, function(error, data) {
    //console.log(data);
    color.domain(d3.keys(data[0]).filter(function(key) { return (key !== "Depth") }));

    data.forEach(function(d) {
      var y0 = 0;
      d.maxTime = max;
      d.times = color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 = +d[name]}; });
      d.total = d.times[d.times.length - 1].y1;
      //d.total = 150;
    });

    data.sort(function(a, b) { return b.total - a.total; });

    x.domain(data.map(function(d) { return d.Depth; }));
    y.domain([0, d3.max(data, function(d) { return d.maxTime; })]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
      //.text("Population");

    var depth = svg.selectAll(".depth")
        .data(data)
      .enter().append("g")
        .attr("class", "g")
        .attr("transform", function(d) { return "translate(" + x(d.Depth) + ",0)"; });

    depth.selectAll("rect")
        .data(function(d) { return d.times; })
      .enter().append("rect")
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.y1); })
        .attr("height", function(d) { return y(d.y0) - y(d.y1); })
        .style("fill", function(d) { return color(d.name); })
        .on('mouseover', function(d){
        tooltip.transition()
          .style('opacity', .9)

        tooltip.html(d.name +"<br>"+ d.y0 + "<br>" + d.y1)
          .style('left', (d3.event.pageX - 35) + 'px')
          .style('top', (d3.event.pageY) + 'px')
        d3.select(this)
          .style('opacity', .5)
      })
      .on('mouseout', function(d){
        d3.select(this)
          .style('opacity', 1)
      });

    var legend = svg.selectAll(".legend")
        .data(color.domain().slice().reverse())
      .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) { return d; });

  });

}
/********************************************************************
DISCLAIMER
THIS SOFTWARE IS A PROTOTYPE AND SHOULD NOT BE USED AS A DIVE PLANNER
OR A REFERENCE FOR DIVE TABLES/INFORMATION FOR ANY DIVER
********************************************************************/